// Express setup
const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port = 4002;

// Mongoose connection
mongoose.connect(`mongodb+srv://yobynnam:admin123@zuittbootcamp-b197pt.ufiwlyz.mongodb.net/readingActListE?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Mongoose to MongoDB
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection'));
db.once('open', () => console.log('Connected to MongoDB!'));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// 1. User schema
const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	username: String,
	password: String,
	email: String
});

// 2. Product schema
const productSchema = new mongoose.Schema({
	name: String,
	description: String,
	price: Number
});

// 3. User and Product model
const User = mongoose.model('User', userSchema);
const Product = mongoose.model('Product', productSchema);

// 4. Create a POST request /register
app.post('/register', (req, res) => {
	// business logic
	User.findOne({user: req.body.username}, (error, result) => {
		if (error) {
			return res.send(error);
		} else if (result != null && result.username == req.body.username) {
			return res.send('Duplicate user found.');
		} else {
			let newUser = new User ({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				username: req.body.username,
				password: req.body.password,
				email: req.body.email
			});
			newUser.save((error, savedUser) => {
				if (error) {
					return console.error(error);
				} else {
					return res.status(201).send('New User registered!');
				};
			});
		};
	});
});

// 5. Create POST request /createProduct
app.post('/createProduct', (req, res) => {
	// business logic
	Product.findOne({name: req.body.name}, (error, result) => {
		if (error) {
			return res.send(error);
		} else if (result != null && result.name == req.body.name) {
			return res.send('Duplicate product.');
		} else {
			let newProduct = new Product ({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price
			});
			newProduct.save((error, savedProduct) => {
				if (error) {
					return console.error(error);
				} else {
					return res.status(201).send('New Product created.');
				};
			});
		};		
	});
});

// 6. Create GET request to /users
app.get('/users', (req, res) => {
	User.find({}, (error, result) => {
		if (error) {
			return res.send(error);
		} else {
			return res.status(200).json({
				user: result
			});
		};
	});
});

// 7. Create GET request to /products
app.get('/products', (req, res) => {
	Product.find({}, (error, result) => {
		if (error) {
			return res.send(error);
		} else {
			return res.status(200).json({
				product: result
			});
		};
	});
});

app.listen(port, () => console.log(`Server is running at port: ${port}.`))